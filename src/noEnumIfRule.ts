import {RuleFailure, RuleWalker} from "tslint";
import {Expression, IfStatement, Program, SourceFile, SyntaxKind, TypeChecker, TypeFlags} from "typescript";
import {IOptions} from "tslint/lib/language/rule/rule";
import {TypedRule} from "tslint/lib/language/rule/typedRule";
import {isBinaryExpression, isParenthesizedExpression} from "tsutils";
import {isNullOrUndefined} from "util";

export class Rule extends TypedRule {
    public static FAILURE_STRING = "compare of enums with if not allowed";

    public applyWithProgram(sourceFile: SourceFile, program: Program): RuleFailure[] {
        // tslint:disable-next-line
        return this.applyWithWalker(new NoEnumIfWalker(sourceFile, this.getOptions(), program.getTypeChecker()));
    }
}

// tslint:disable:max-classes-per-file
class NoEnumIfWalker extends RuleWalker {
    constructor(sourceFile: SourceFile, options: IOptions, private readonly checker: TypeChecker) {
        super(sourceFile, options);
    }

    public visitIfStatement(node: IfStatement): void {
        if (!isBinaryExpression(node.expression)) {
            return;
        }

        if (this.checkEveryCondition(node.expression)) {
            this.addFailureAt(node.expression.getStart(), node.expression.getWidth(), Rule.FAILURE_STRING);
        }

        if (!isNullOrUndefined(node.elseStatement) && node.elseStatement.kind === SyntaxKind.IfStatement) {
            //console.log(node.elseStatement as IfStatement)
            this.visitIfStatement(node.elseStatement as IfStatement);
        }


    }

    private checkEveryCondition(node: Expression): boolean {
        if (!isBinaryExpression(node)) {
            if (isParenthesizedExpression(node)) {
                let rc = true;
                // console.log((node.expression as any).left);
                // console.log(rc);
                //console.log((node as Expression).getText());
                rc = rc && this.checkEveryCondition((node.expression as Expression));
                // console.log(rc);
                // rc = rc && this.checkEveryCondition((node.expression as any).right);
                // console.log(rc);
                // node.expression.getChildren().forEach(child=> {
                //     console.log(child as any);
                //     rc = rc && this.checkEveryCondition((child as any).expression);
                // });

                return rc;
            } else {
                // console.log((node as Expression).getText());
                // console.log(isExpression(node));
                // console.log(isParenthesizedExpression(node));
                // console.log(node);

                return false;
            }
        }

        const {operatorToken, left, right} = node;
        switch (operatorToken.kind) {
            case SyntaxKind.AmpersandAmpersandToken:
            case SyntaxKind.BarBarToken:
                return this.checkEveryCondition(left) || this.checkEveryCondition(right);
            case SyntaxKind.EqualsEqualsToken:
            case SyntaxKind.ExclamationEqualsToken:
            case SyntaxKind.EqualsEqualsEqualsToken:
            case SyntaxKind.ExclamationEqualsEqualsToken:
                const oneIsEnum = this.isEnum(left) || this.isEnum(right);
                const bothAreVariable = this.isVariable(left) && this.isVariable(right);

                return oneIsEnum && !bothAreVariable;
            default:
                //console.error(operatorToken.kind);
                return false;
        }
    }

    private isVariable(expression: Expression): boolean {
        const type = this.checker.getSymbolAtLocation(expression);
        if (isNullOrUndefined(type)) {
            return false;
        }

        // tslint:disable:no-bitwise
        const isAny = (type.flags & TypeFlags.Any) !== 0;

        return isAny;
    }

    private isEnum(expression: Expression): boolean {
        const type = this.checker.getTypeAtLocation(expression);
        // tslint:disable:no-bitwise
        const isEnum = (type.flags & TypeFlags.EnumLiteral) !== 0;

        return isEnum;
    }
}
