import {RuleFailure, RuleWalker} from "tslint";
import {ConditionalExpression, Program, SourceFile,} from "typescript";
import {IOptions} from "tslint/lib/language/rule/rule";
import {TypedRule} from "tslint/lib/language/rule/typedRule";

export class Rule extends TypedRule {
    public static FAILURE_STRING = "ternary operator not allowed";

    public applyWithProgram(sourceFile: SourceFile, program: Program): RuleFailure[] {
        // tslint:disable-next-line
        return this.applyWithWalker(new NoTernaryOperatorWalker(sourceFile, this.getOptions()));
    }
}

// tslint:disable:max-classes-per-file
class NoTernaryOperatorWalker extends RuleWalker {
    constructor(sourceFile: SourceFile, options: IOptions) {
        super(sourceFile, options);
    }

    public visitConditionalExpression(node: ConditionalExpression): void {
        // console.log(node.condition.getText());
        // console.log(node.questionToken.getText());
        this.addFailureAt(node.questionToken.getStart(), node.questionToken.getWidth(), Rule.FAILURE_STRING);
    }

}
