import {RuleFailure, RuleWalker} from "tslint";
import {
    DefaultClause,
    Expression,
    Program,
    SourceFile,
    SwitchStatement,
    SyntaxKind,
    TypeChecker,
    TypeFlags
} from "typescript";
import {IOptions} from "tslint/lib/language/rule/rule";
import {TypedRule} from "tslint/lib/language/rule/typedRule";

//TODO change this, so that it is only active for enums
export class Rule extends TypedRule {
    public static FAILURE_STRING = "default statement in switch not allowed";

    public applyWithProgram(sourceFile: SourceFile, program: Program): RuleFailure[] {
        // tslint:disable-next-line
        return this.applyWithWalker(new NoSwitchDefaultWalker(sourceFile, this.getOptions(), program.getTypeChecker()));
    }
}

// tslint:disable:max-classes-per-file
class NoSwitchDefaultWalker extends RuleWalker {
    constructor(sourceFile: SourceFile, options: IOptions, private readonly checker: TypeChecker) {
        super(sourceFile, options);
    }

    public visitSwitchStatement(node: SwitchStatement): void {

        if (!this.isEnum(node.expression)) {
            return;
        }

        node.caseBlock.clauses
            .filter((c) => c.kind === SyntaxKind.DefaultClause)
            .forEach((clause) => {
                this.addFailureAt(clause.statements[0].getStart(), clause.statements[0].getWidth(), Rule.FAILURE_STRING);
            });

    }

    public visitDefaultClause(node: DefaultClause): void {
        // console.log(node);
        this.addFailureAt(node.statements[0].getStart(), node.statements[0].getWidth(), Rule.FAILURE_STRING);
    }

    private isEnum(expression: Expression): boolean {
        const type = this.checker.getTypeAtLocation(expression);

        // tslint:disable:no-bitwise
        return (type.flags & TypeFlags.EnumLiteral) !== 0;
    }
}
