import {RuleFailure, RuleWalker} from "tslint";
import {
    CaseClause,
    Expression,
    Program,
    SourceFile,
    SwitchStatement,
    SyntaxKind,
    TypeChecker,
    TypeFlags
} from "typescript";
import {IOptions} from "tslint/lib/language/rule/rule";
import {TypedRule} from "tslint/lib/language/rule/typedRule";

export class Rule extends TypedRule {

    public applyWithProgram(sourceFile: SourceFile, program: Program): RuleFailure[] {
        // tslint:disable-next-line
        return this.applyWithWalker(new AllSwitchEnumOptionsCoveredWalker(sourceFile, this.getOptions(), program.getTypeChecker()));
    }
}

class AllSwitchEnumOptionsCoveredWalker extends RuleWalker {
    constructor(sourceFile: SourceFile, options: IOptions, private readonly checker: TypeChecker) {
        super(sourceFile, options);
    }

    public visitSwitchStatement(node: SwitchStatement): void {

        if (!this.isEnum(node.expression)) {
            return;
        }

        const missingKeys: string[] = [];
        const keysFromEnum: string[] = [];
        const keysFromSwitch: string[] = [];

        const type = this.checker.getTypeAtLocation(node.expression);
        this.extractEnumKeys(type, keysFromEnum);

        node.caseBlock.clauses
            .filter((c) => c.kind !== SyntaxKind.DefaultClause)
            .forEach((clause: CaseClause) => {
                // tslint:disable-next-line
                keysFromSwitch.push((clause.expression.getLastToken() as any).escapedText);
            });

        // console.log(keysFromEnum);
        // console.log(keysFromSwitch);

        keysFromEnum
            .filter((entry) => keysFromSwitch.indexOf(entry) === -1)
            .forEach((missing) => missingKeys.push(missing));

        if (missingKeys.length !== 0) {
            const errorMessage = "Not all enum entries covered, missing: " + missingKeys.join(', ');
            this.addFailureAt(node.expression.getStart(), node.expression.getWidth(), errorMessage);
        }

    }

    private extractEnumKeys(type, keysFromEnum: string[]): void {
        const keyIterator = type.symbol.exports.keys();
        let value = keyIterator.next();
        while (!value.done) {
            keysFromEnum.push(value.value.toString());
            value = keyIterator.next();
        }
    }

    private isEnum(expression: Expression): boolean {
        const type = this.checker.getTypeAtLocation(expression);

        // tslint:disable:no-bitwise
        return (type.flags & TypeFlags.EnumLiteral) !== 0;
    }
}
