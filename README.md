# Guideline Checkers for TS Lint

## Development
* Run ```npm run compile``` and copy the ```*.js```-files from ```dist/out-tsc``` to the project you want to use them in.
* use the ```dev:<rule>``` configurations to run the rules from ```dist/out-tsc``` onto the test files.

## Checkers

### allSwitchEnumOptionsCovered

### noEnumIf

### noEnumSwitchDefault

### noTernaryOperator
Checks if the ?-operator is used.